﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using FluentAssertions;
using Logging;
using Xunit;

namespace LoggingDotNetFrameWork.Tests
{
    [Category("IntegrationTest")]
    public class LogProviderTests : IDisposable
    {
        private const string fileName = "testlogfile.txt";
        private static readonly ILog sut;

        static LogProviderTests()
        {
            sut = LogProvider.For<LogProviderTests>();
        }

        [Fact]
        public void Log4Net_Provider_Should_Work_Properly()
        {
            // Arrange
            var expected = new[]
            {
                    "DEBUG LoggingDotNetFrameWork.Tests.LogProviderTests - wrapper: Message For Debug",
                    "INFO LoggingDotNetFrameWork.Tests.LogProviderTests - wrapper: Message For Info",
                    "WARN LoggingDotNetFrameWork.Tests.LogProviderTests - wrapper: Message For Warning",
                    "ERROR LoggingDotNetFrameWork.Tests.LogProviderTests - wrapper: Message For Error",
                    "FATAL LoggingDotNetFrameWork.Tests.LogProviderTests - wrapper: Message For Fatal"
                };
            // Act
            sut.Debug("wrapper: Message For Debug");
            sut.Info("wrapper: Message For Info");
            sut.Warn("wrapper: Message For Warning");
            sut.Error("wrapper: Message For Error");
            sut.Fatal("wrapper: Message For Fatal");

            log4net.LogManager.ShutdownRepository();

            var actual = File.ReadAllText(fileName);

            // Assert
            sut.IsDebugEnabled().Should().BeTrue();
            sut.IsInfoEnabled().Should().BeTrue();
            sut.IsWarnEnabled().Should().BeTrue();
            sut.IsErrorEnabled().Should().BeTrue();
            sut.IsFatalEnabled().Should().BeTrue();

            expected.All(a => actual.Contains(a)).Should().BeTrue();
        }

        public void Dispose()
        {
            if (File.Exists(fileName))
                File.Delete(fileName);
        }
    }
}
