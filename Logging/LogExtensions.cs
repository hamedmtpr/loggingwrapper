﻿using System;

namespace Logging
{
    public static class LogExtensions
    {
        #region Public Methods

        public static bool IsDebugEnabled(this ILog logger)
        {
            guardAgainstNullLogger(logger);
            return logger.Log(LogLevel.Debug, null);
        }

        public static bool IsErrorEnabled(this ILog logger)
        {
            guardAgainstNullLogger(logger);
            return logger.Log(LogLevel.Error, null);
        }

        public static bool IsFatalEnabled(this ILog logger)
        {
            guardAgainstNullLogger(logger);
            return logger.Log(LogLevel.Fatal, null);
        }

        public static bool IsInfoEnabled(this ILog logger)
        {
            guardAgainstNullLogger(logger);
            return logger.Log(LogLevel.Info, null);
        }

        public static bool IsTraceEnabled(this ILog logger)
        {
            guardAgainstNullLogger(logger);
            return logger.Log(LogLevel.Trace, null);
        }

        public static bool IsWarnEnabled(this ILog logger)
        {
            guardAgainstNullLogger(logger);
            return logger.Log(LogLevel.Warn, null);
        }

        public static void Debug(this ILog logger, Func<string> messageFunc)
        {
            guardAgainstNullLogger(logger);
            logger.Log(LogLevel.Debug, messageFunc);
        }

        public static void Debug(this ILog logger, string message)
        {
            if (!logger.IsDebugEnabled())
                return;
            logger.Log(LogLevel.Debug, message.AsFunc());
        }

        public static void DebugFormat(this ILog logger, string message, params object[] args)
        {
            if (!logger.IsDebugEnabled())
                return;
            logger.LogFormat(LogLevel.Debug, message, args);
        }

        public static void DebugException(this ILog logger, string message, Exception exception)
        {
            if (!logger.IsDebugEnabled())
                return;
            logger.Log(LogLevel.Debug, message.AsFunc(), exception);
        }

        public static void DebugException(
            this ILog logger,
            string message,
            Exception exception,
            params object[] formatParams)
        {
            if (!logger.IsDebugEnabled())
                return;
            logger.Log(LogLevel.Debug, message.AsFunc(), exception, formatParams);
        }

        public static void Error(this ILog logger, Func<string> messageFunc)
        {
            guardAgainstNullLogger(logger);
            logger.Log(LogLevel.Error, messageFunc);
        }

        public static void Error(this ILog logger, string message)
        {
            if (!logger.IsErrorEnabled())
                return;
            logger.Log(LogLevel.Error, message.AsFunc());
        }

        public static void ErrorFormat(this ILog logger, string message, params object[] args)
        {
            if (!logger.IsErrorEnabled())
                return;
            logger.LogFormat(LogLevel.Error, message, args);
        }

        public static void ErrorException(
            this ILog logger,
            string message,
            Exception exception,
            params object[] formatParams)
        {
            if (!logger.IsErrorEnabled())
                return;
            logger.Log(LogLevel.Error, message.AsFunc(), exception, formatParams);
        }

        public static void Fatal(this ILog logger, Func<string> messageFunc)
        {
            logger.Log(LogLevel.Fatal, messageFunc);
        }

        public static void Fatal(this ILog logger, string message)
        {
            if (!logger.IsFatalEnabled())
                return;
            logger.Log(LogLevel.Fatal, message.AsFunc());
        }

        public static void FatalFormat(this ILog logger, string message, params object[] args)
        {
            if (!logger.IsFatalEnabled())
                return;
            logger.LogFormat(LogLevel.Fatal, message, args);
        }

        public static void FatalException(
            this ILog logger,
            string message,
            Exception exception,
            params object[] formatParams)
        {
            if (!logger.IsFatalEnabled())
                return;
            logger.Log(LogLevel.Fatal, message.AsFunc(), exception, formatParams);
        }

        public static void Info(this ILog logger, Func<string> messageFunc)
        {
            guardAgainstNullLogger(logger);
            logger.Log(LogLevel.Info, messageFunc);
        }

        public static void Info(this ILog logger, string message)
        {
            if (!logger.IsInfoEnabled())
                return;
            logger.Log(LogLevel.Info, message.AsFunc());
        }

        public static void InfoFormat(this ILog logger, string message, params object[] args)
        {
            if (!logger.IsInfoEnabled())
                return;
            logger.LogFormat(LogLevel.Info, message, args);
        }

        public static void InfoException(
            this ILog logger,
            string message,
            Exception exception,
            params object[] formatParams)
        {
            if (!logger.IsInfoEnabled())
                return;
            logger.Log(LogLevel.Info, message.AsFunc(), exception, formatParams);
        }

        public static void Trace(this ILog logger, Func<string> messageFunc)
        {
            guardAgainstNullLogger(logger);
            logger.Log(LogLevel.Trace, messageFunc);
        }

        public static void Trace(this ILog logger, string message)
        {
            if (!logger.IsTraceEnabled())
                return;
            logger.Log(LogLevel.Trace, message.AsFunc());
        }

        public static void TraceFormat(this ILog logger, string message, params object[] args)
        {
            if (!logger.IsTraceEnabled())
                return;
            logger.LogFormat(LogLevel.Trace, message, args);
        }

        public static void TraceException(
            this ILog logger,
            string message,
            Exception exception,
            params object[] formatParams)
        {
            if (!logger.IsTraceEnabled())
                return;
            logger.Log(LogLevel.Trace, message.AsFunc(), exception, formatParams);
        }

        public static void Warn(this ILog logger, Func<string> messageFunc)
        {
            guardAgainstNullLogger(logger);
            logger.Log(LogLevel.Warn, messageFunc);
        }

        public static void Warn(this ILog logger, string message)
        {
            if (!logger.IsWarnEnabled())
                return;
            logger.Log(LogLevel.Warn, message.AsFunc());
        }

        public static void WarnFormat(this ILog logger, string message, params object[] args)
        {
            if (!logger.IsWarnEnabled())
                return;
            logger.LogFormat(LogLevel.Warn, message, args);
        }

        public static void WarnException(
            this ILog logger,
            string message,
            Exception exception,
            params object[] formatParams)
        {
            if (!logger.IsWarnEnabled())
                return;
            logger.Log(LogLevel.Warn, message.AsFunc(), exception, formatParams);
        }

        #endregion

        #region Private Methods

        private static void guardAgainstNullLogger(ILog logger)
        {
            if (logger == null)
                throw new ArgumentNullException(nameof(logger));
        }

        private static void LogFormat(this ILog logger,
            LogLevel logLevel,
            string message,
            params object[] args)
        {
            logger.Log(logLevel, message.AsFunc(), null, args);
        }

        private static Func<T> AsFunc<T>(this T value) where T : class
        {
            return value.Return<T>;
        }

        private static T Return<T>(this T value)
        {
            return value;
        }

        #endregion
    }
}