﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Logging.LogProviders;

namespace Logging
{
    public static partial class LogProvider
    {
        #region Statics

        private const string NullLogProvider = "Current Log Provider is not set. Call SetCurrentLogProvider with a non-null value first.";

        private static dynamic currentLogProvider;

        private static Action<ILogProvider> onCurrentLogProviderSet;

        #endregion

        #region Properties

        public static bool IsDisabled { get; }
        internal static List<Tuple<IsLoggerAvailable, CreateLogProvider>> LogProviderResolvers { get; }

        #endregion

        #region Delegates

        internal delegate bool IsLoggerAvailable();

        internal delegate ILogProvider CreateLogProvider();

        #endregion;

        #region Constructors

        static LogProvider()
        {
            IsDisabled = false;

            LogProviderResolvers = new[]
            {
                new Tuple<IsLoggerAvailable, CreateLogProvider>(Log4NetLogProvider.IsLoggerAvailable, () =>  new Log4NetLogProvider())
                //new Tuple<IsLoggerAvailable, CreateLogProvider>(new IsLoggerAvailable(NLogLogProvider.IsLoggerAvailable), (CreateLogProvider) (() => (ILogProvider) new NLogLogProvider())), // TODO Implement Other Provider
                //new Tuple<IsLoggerAvailable, CreateLogProvider>(SerilogLogProvider.IsLoggerAvailable, () => (ILogProvider) new SerilogLogProvider()),
                //new Tuple<IsLoggerAvailable, CreateLogProvider>(EntLibLogProvider.IsLoggerAvailable, () => (ILogProvider) new EntLibLogProvider()),
                //new Tuple<IsLoggerAvailable, CreateLogProvider>(LoupeLogProvider.IsLoggerAvailable, () => (ILogProvider) new LoupeLogProvider())
            }.ToList();
        }

        #endregion

        #region Public Methods

        public static ILog For<T>()
        {
            return GetLogger(typeof(T));
        }

        public static void SetCurrentLogProvider(ILogProvider logProvider)
        {
            currentLogProvider = logProvider;
            RaiseOnCurrentLogProviderSet();
        }

        public static ILog GetLogger(Type type, string fallbackTypeName = "System.Object")
        {
            return GetLogger((object)type != null ? type.FullName : fallbackTypeName);
        }

        public static ILog GetLogger(string name)
        {
            var logProvider = CurrentLogProvider ?? ResolveLogProvider();
            return logProvider == null ? (ILog)NoOpLogger.Instance : new LoggerExecutionWrapper(logProvider.GetLogger(name), () => IsDisabled);
        }

        public static IDisposable OpenNestedContext(string message)
        {
            var logProvider = CurrentLogProvider ?? ResolveLogProvider();
            return logProvider == null ? new DisposableAction(() => { }) : logProvider.OpenNestedContext(message);
        }

        public static IDisposable OpenMappedContext(string key, string value)
        {
            var logProvider = CurrentLogProvider ?? ResolveLogProvider();
            return logProvider == null ? new DisposableAction(() => { }) : logProvider.OpenMappedContext(key, value);
        }

        #endregion

        #region Internal Methods
        
        internal static ILogProvider CurrentLogProvider => currentLogProvider as ILogProvider;

        internal static Action<ILogProvider> OnCurrentLogProviderSet
        {
            set
            {
                onCurrentLogProviderSet = value;
                RaiseOnCurrentLogProviderSet();
            }
        }

        internal static ILogProvider ResolveLogProvider()
        {
            try
            {
                foreach (var providerResolver in LogProviderResolvers.Where(providerResolver => providerResolver.Item1()))
                    return providerResolver.Item2();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception occurred resolving a log provider. Logging for this assembly {0} is disabled. {1}",
                    typeof(LogProvider).GetAssemblyPortable().FullName as object,
                    ex as object);
            }

            return null;
        }

        #endregion

        #region Private Methods

        private static void RaiseOnCurrentLogProviderSet()
        {
            if (onCurrentLogProviderSet == null)
                return;

            onCurrentLogProviderSet(currentLogProvider);
        } 

        #endregion
    }
}