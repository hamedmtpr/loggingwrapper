﻿using System;

namespace Logging.LogProviders
{
    internal class DisposableAction : IDisposable
    {
        private readonly Action onDispose;

        public DisposableAction(Action onDispose = null)
        {
            this.onDispose = onDispose;
        }

        public void Dispose()
        {
            onDispose?.Invoke();
        }
    }
}
