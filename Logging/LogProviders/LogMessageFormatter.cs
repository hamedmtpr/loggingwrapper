﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Logging.LogProviders
{
    internal static class LogMessageFormatter
    {
        #region Statics

        private static readonly Regex pattern = new Regex("(?<!{){@?(?<arg>[^\\d{][^ }]*)}");

        #endregion

        #region Public Methods

        public static Func<string> SimulateStructuredLogging(
            Func<string> messageBuilder,
           object[] formatParameters)
        {
            if (formatParameters == null || formatParameters.Length == 0)
                return messageBuilder;

            return () => FormatStructuredMessage(messageBuilder(), formatParameters, out var patternMatches);
        }

        public static string FormatStructuredMessage(
            string targetMessage,
            object[] formatParameters,
            out IEnumerable<string> patternMatches)
        {
            if (formatParameters.Length == 0)
            {
                patternMatches = Enumerable.Empty<string>();
                return targetMessage;
            }

            var stringList = new List<string>();
            patternMatches = stringList;

            foreach (Match match in pattern.Matches(targetMessage))
            {
                var s = match.Groups["arg"].Value;

                if (int.TryParse(s, out var result))
                    continue;

                var stringIndex = stringList.IndexOf(s);

                if (stringIndex == -1)
                {
                    stringIndex = stringList.Count;
                    stringList.Add(s);
                }

                targetMessage =
                    replaceFirst(targetMessage,
                        match.Value,
                        "{" + stringIndex + match.Groups["format"].Value + "}");
            }

            try
            {
                return string.Format(CultureInfo.InvariantCulture, targetMessage, formatParameters);
            }
            catch (FormatException ex)
            {
                throw new FormatException($"The input string '{targetMessage}' could not be formatted using string.Format", ex);
            }
        }

        #endregion

        #region Private Methods
        
        private static string replaceFirst(string text, string search, string replace)
        {
            var index = text.IndexOf(search, StringComparison.Ordinal);

            return index >= 0
                ? (text.Substring(0, index) + replace + text.Substring(index + search.Length))
                : text;
        } 

        #endregion

    }
}