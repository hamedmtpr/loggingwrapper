﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Logging.LogProviders
{
    internal static class TypeExtensions
    {
        internal static ConstructorInfo GetConstructorPortable(
            this Type type,
            params Type[] types)
        {
            return type.GetConstructors()
                .FirstOrDefault(f => 
                    f.GetParameters()
                        .Select(s => s.ParameterType)
                        .SequenceEqual(types));
        }

        internal static MethodInfo GetMethodPortable(this Type type, string name)
        {
            return type.GetMethods()
                .FirstOrDefault(f => f.Name == name);
        }

        internal static MethodInfo GetMethodPortable(
            this Type type,
            string name,
            params Type[] types)
        {
            return type.GetMethod(name, types);
        }

        internal static PropertyInfo GetPropertyPortable(this Type type, string name)
        {
            return type.GetProperty(name);
        }

        internal static IEnumerable<FieldInfo> GetFieldsPortable(this Type type)
        {
            return  type.GetFields();
        }

        internal static Type GetBaseTypePortable(this Type type)
        {
            return type.BaseType;
        }

        internal static MethodInfo GetGetMethod(this PropertyInfo propertyInfo)
        {
            return propertyInfo.GetGetMethod();
        }

        internal static MethodInfo GetSetMethod(this PropertyInfo propertyInfo)
        {
            return propertyInfo.GetSetMethod();
        }

        internal static Assembly GetAssemblyPortable(this Type type)
        {
            return Assembly.GetAssembly(type);
        }
    }
}