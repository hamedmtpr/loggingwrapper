﻿using System;
using System.IO;
using System.Reflection;

namespace Logging.LogProviders
{
    internal partial class Log4NetLogProvider : LogProviderBase
    {
        #region Statics

        private static readonly Type logManagerType = Type.GetType("log4net.LogManager, log4net");
        private static readonly Type iLoggerRepositoryType = Type.GetType("log4net.Repository.ILoggerRepository, log4net");
        private static readonly Type logicalThreadContextType = Type.GetType("log4net.LogicalThreadContext, log4net");

        #endregion

        #region Properties

        public static bool ProviderIsAvailableOverride { get; }

        #endregion

        #region Constructors

        public Log4NetLogProvider()
        {
            if (!IsLoggerAvailable())
                throw new InvalidOperationException("log4net.LogManager not found");
        }

        static Log4NetLogProvider()
        {
            ProviderIsAvailableOverride = true;

            initializeConfiguration();
        }

        #endregion

        #region Public Methods

        public override Logger GetLogger(string name)
        {
            return new Log4NetLogger(logManagerType.GetMethodPortable("GetLogger",
                    typeof(string), 
                    typeof(string)).Invoke(null,
                    new[]
                    {
                        getLoggerRepository().Name,
                        name,
                    })).Log;
        }

        #endregion

        #region Internal Methods

        internal static bool IsLoggerAvailable()
        {
            return ProviderIsAvailableOverride && logManagerType != null;
        }

        #endregion

        #region Protected Methods

        protected override OpenNdc GetOpenNdcMethod()
        {
            throw new NotImplementedException(); // TODO Implement
        }

        protected override OpenMdc GetOpenMdcMethod()
        {
            throw new NotImplementedException(); // TODO Implement
        }

        #endregion

        #region Private Methods

        private static void initializeConfiguration()
        {
            Type.GetType("log4net.Config.XmlConfigurator, log4net")
                .GetMethodPortable("ConfigureAndWatch", iLoggerRepositoryType, typeof(FileInfo))
                .Invoke(null, new[]
                {
                    getLoggerRepository(),
                    new FileInfo("log4net.config"),
                });
        }

        private static dynamic getLoggerRepository()
        {
            return logManagerType
                  .GetMethodPortable("GetRepository", typeof(Assembly))
                  .Invoke(null, new[]
                  {
                    iLoggerRepositoryType.GetAssemblyPortable(),
                  });
        }

        #endregion
    }
}