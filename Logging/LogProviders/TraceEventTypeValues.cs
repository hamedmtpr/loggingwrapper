﻿using System;
using System.Reflection;

namespace Logging.LogProviders
{
    internal static class TraceEventTypeValues
    {
        internal static readonly Type Type;
        internal static readonly int Verbose;
        internal static readonly int Information;
        internal static readonly int Warning;
        internal static readonly int Error;
        internal static readonly int Critical;

        static TraceEventTypeValues()
        {
            Assembly assemblyPortable = typeof(Uri).GetAssemblyPortable();
            if ((object)assemblyPortable == null)
                return;
            TraceEventTypeValues.Type = assemblyPortable.GetType("System.Diagnostics.TraceEventType");
            if ((object)TraceEventTypeValues.Type == null)
                return;
            TraceEventTypeValues.Verbose = (int)Enum.Parse(TraceEventTypeValues.Type, nameof(Verbose), false);
            TraceEventTypeValues.Information = (int)Enum.Parse(TraceEventTypeValues.Type, nameof(Information), false);
            TraceEventTypeValues.Warning = (int)Enum.Parse(TraceEventTypeValues.Type, nameof(Warning), false);
            TraceEventTypeValues.Error = (int)Enum.Parse(TraceEventTypeValues.Type, nameof(Error), false);
            TraceEventTypeValues.Critical = (int)Enum.Parse(TraceEventTypeValues.Type, nameof(Critical), false);
        }
    }
}