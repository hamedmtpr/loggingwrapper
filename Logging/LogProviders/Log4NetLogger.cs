﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Logging.LogProviders
{
    internal partial class Log4NetLogProvider
    {
        private class Log4NetLogger
        {
            #region Statics

            private static readonly object callerStackBoundaryTypeSync = new object();
            private static Type callerStackBoundaryType;

            private static readonly Type loggingEventType;

            private static readonly Dictionary<LogLevel, dynamic> logLevelToLog4netLogLevelMap;

            #endregion

            #region Field

            private readonly dynamic logger;

            #endregion

            #region Constructors

            static Log4NetLogger()
            {
                loggingEventType = getLog4NetType("log4net.Core.LoggingEvent");

                var levelTypeFieldsInfo = getLog4NetType("log4net.Core.Level").GetFieldsPortable().ToList();

                logLevelToLog4netLogLevelMap = new Dictionary<LogLevel, dynamic>()
                {
                    [LogLevel.Trace] = levelTypeFieldsInfo.First(x => x.Name == "Debug").GetValue(null),
                    [LogLevel.Debug] = levelTypeFieldsInfo.First(x => x.Name == "Debug").GetValue(null),
                    [LogLevel.Info] = levelTypeFieldsInfo.First(x => x.Name == "Info").GetValue(null),
                    [LogLevel.Warn] = levelTypeFieldsInfo.First(x => x.Name == "Warn").GetValue(null),
                    [LogLevel.Error] = levelTypeFieldsInfo.First(x => x.Name == "Error").GetValue(null),
                    [LogLevel.Fatal] = levelTypeFieldsInfo.First(x => x.Name == "Fatal").GetValue(null),

                };
            }

            internal Log4NetLogger(dynamic logger)
            {
                this.logger = logger.Logger;
            }

            #endregion

            #region Public Methods

            public bool Log(LogLevel logLevel,
                Func<string> messageFunc,
                Exception exception,
                params object[] formatParameters)
            {
                if (messageFunc == null)
                    return logger.IsEnabledFor(translateLevel(logLevel));

                if (!logger.IsEnabledFor(translateLevel(logLevel)))
                    return false;

                lock (callerStackBoundaryTypeSync)
                    callerStackBoundaryType = typeof(LoggerExecutionWrapper);

                var formatStructuredMessage = LogMessageFormatter.FormatStructuredMessage(messageFunc(),
                    formatParameters,
                    out var patternMatches);

                dynamic loggingEvent = Activator.CreateInstance(loggingEventType,
                    new[]
                    {
                        callerStackBoundaryType,
                        logger.Repository,
                        logger.Name,
                        translateLevel(logLevel),
                        formatStructuredMessage,
                        exception,
                    });

                populateProperties(loggingEvent, patternMatches, formatParameters);

                logger.Log(loggingEvent);

                return true;
            }

            #endregion

            #region Private Methods

            private static Type getLog4NetType(string type)
            {
                var levelType = Type.GetType($"{type}, log4net");

                if (levelType == null)
                    throw new InvalidOperationException($"Type {type} was not found.");


                return levelType;
            }

            private void populateProperties(
                object loggingEvent,
                IEnumerable<string> patternMatches,
                IEnumerable<object> formatParameters)
            {
                foreach (var keyValuePair in patternMatches.Zip(formatParameters,
                    (key, value) => new KeyValuePair<string, object>(key, value)))
                    logger.Repository.Properties.Add(keyValuePair.Key, keyValuePair.Value);
            }

            private dynamic translateLevel(LogLevel logLevel)
            {
                try
                {
                    return logLevelToLog4netLogLevelMap[logLevel];
                }
                catch (Exception)
                {
                    throw new ArgumentOutOfRangeException(nameof(logLevel), logLevel, null);
                }
            }

            #endregion
        }
    }
}