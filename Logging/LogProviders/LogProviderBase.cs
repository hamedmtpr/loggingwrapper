﻿using System;

namespace Logging.LogProviders
{
    internal abstract class LogProviderBase : ILogProvider
    {
        #region Fields
        
        private static readonly IDisposable noOpDisposableInstance = new DisposableAction();

        private readonly Lazy<OpenNdc> openNdcMethod;
        private readonly Lazy<OpenMdc> openMdcMethod;

        protected delegate IDisposable OpenNdc(string message);
        protected delegate IDisposable OpenMdc(string key, string value);

        #endregion

        #region Constructors
        
        protected LogProviderBase()
        {
            openNdcMethod = new Lazy<OpenNdc>(GetOpenNdcMethod);
            openMdcMethod = new Lazy<OpenMdc>(GetOpenMdcMethod);
        }

        #endregion

        #region Public Methods
        
        public abstract Logger GetLogger(string name);

        public IDisposable OpenNestedContext(string message)
        {
            return openNdcMethod.Value(message);
        }

        public IDisposable OpenMappedContext(string key, string value)
        {
            return openMdcMethod.Value(key, value);
        }

        #endregion

        #region Protected Methods
        
        protected virtual OpenNdc GetOpenNdcMethod()
        {
            return message => noOpDisposableInstance;
        }

        protected virtual OpenMdc GetOpenMdcMethod()
        {
            return (key, value) => noOpDisposableInstance;
        } 

        #endregion
    }
}