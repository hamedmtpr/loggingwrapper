﻿using System;

namespace Logging
{
    internal class LoggerExecutionWrapper : ILog
    {
        #region Fields

        private const string FailedToGenerateLogMessage = "Failed to generate log message";
        private readonly Func<bool> getIsDisabled;

        #endregion

        #region Properties

        internal Logger WrappedLogger { get; }

        #endregion

        #region Constructors

        internal LoggerExecutionWrapper(Logger logger, Func<bool> getIsDisabled = null)
        {
            WrappedLogger = logger;
            this.getIsDisabled = getIsDisabled ?? (() => false);
        }

        #endregion

        #region public Methods

        public bool Log(LogLevel logLevel,
            Func<string> messageFunc,
            Exception exception = null,
            params object[] formatParameters)
        {
            if (getIsDisabled())
                return false;

            if (messageFunc == null)
                return WrappedLogger(logLevel, null);

            return WrappedLogger(logLevel, () =>
            {
                try
                {
                    return messageFunc();
                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, () => FailedToGenerateLogMessage, ex);
                }
                return null;
            }, exception, formatParameters);
        }

        #endregion
    }
}